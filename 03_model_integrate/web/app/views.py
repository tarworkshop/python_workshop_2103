from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt

from .sentiment.core import find_sentiment

def index(request):
    context = {}
    if request.method == 'POST':
        message = request.POST.get('message')
        sentiment = find_sentiment(message)[0]
        context = {'message': message, 'sentiment': sentiment}
    return render(request, 'index.html', context)

@csrf_exempt
def sentiment_api(request):
    if request.method == 'POST':
        message = request.POST.get('message')
        sentiment = find_sentiment(message)[0]
        return JsonResponse({'message': message, 'sentiment': sentiment}, status=200)
    else:
        return HttpResponse(status=400)
