import re
from pythainlp.tokenize import word_tokenize

def pre_tokenize(message):
    #     
    # Remove URL in string
    # 
    message = re.sub(r'(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))', '', message)
    
    #     
    # Keep only thai letter and englist letter
    # 
    pattern = re.compile(r"[^\u0E00-\u0E7Fa-zA-Z' ]|^'|'$|''")
    char_to_remove = re.findall(pattern, message)
    message = "".join([char for char in message if not char in char_to_remove])
    
    return message

def post_tokenize(tokens):
    nt = []
    for tok in tokens:
        t = tok.strip()
        if t: 
            nt.append(t)
    return nt
        
def custom_tokenizer(message):
    message = pre_tokenize(message)
    tokens = word_tokenize(message)
    tokens = post_tokenize(tokens)
    return tokens