import os

from joblib import load
from django.conf import settings

est = load(os.path.join(settings.BASE_DIR, 'model', 'sentiment.joblib'))
vectorizer = load(os.path.join(settings.BASE_DIR, 'model', 'sentiment_vect.joblib'))

def find_sentiment(message):
    vect = vectorizer.transform([message])
    pred = est.predict(vect)
    return pred